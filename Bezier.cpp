#include "Bezier.h"
#include "Line.h"
#include <iostream>
using namespace std;

Vector2D CalculateBezierPoint2(float t, Vector2D p0, Vector2D p1, Vector2D p2)
{
	float u = 1 - t;
	float tt = t*t;
	float uu = u*u;

	Vector2D p;
	p.x = uu*p0.x + 2 * u*t*p1.x + tt*p2.x;
	p.y = uu*p0.y + 2 * u*t*p1.y + tt*p2.y;

	return p;
}
void DrawCurve2(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3)
{
	float t = 0;
	int n = 100000;
	Vector2D q0 = CalculateBezierPoint2(t, p1, p2, p3);
	Vector2D q1;
	for (int i = 1; i <= n; i++)
	{
		t = i / (float)n;
		q1 = CalculateBezierPoint2(t, p1, p2, p3);
		Bresenham_Line(q0.x, q0.y, q1.x, q1.y, ren);
		q0 = q1;
	}
}
Vector2D CalculateBezierPoint3(float t, Vector2D p0, Vector2D p1, Vector2D p2, Vector2D p3)
{
	float u = 1 - t;
	float tt = t*t;
	float ttt = tt*t;
	float uu = u*u;
	float uuu = uu*u;

	Vector2D p;
	p.x = uuu*p0.x + 3 * uu*t*p1.x + 3 * u*tt*p2.x + ttt*p3.x;
	p.y = uuu*p0.y + 3 * uu*t*p1.y + 3 * u*tt*p2.y + ttt*p3.y;

	return p;
}
void DrawCurve3(SDL_Renderer *ren, Vector2D p1, Vector2D p2, Vector2D p3, Vector2D p4)
{ 
	float t = 0;
	int n = 100000;
	Vector2D q0 = CalculateBezierPoint3(t, p1, p2, p3, p4);
	Vector2D q1;
	for (int i = 1; i <= n; i++)
	{
		t = i / (float)n;
		q1 = CalculateBezierPoint3(t, p1, p2, p3, p4);
		Bresenham_Line(q0.x, q0.y, q1.x, q1.y, ren);
		q0 = q1;
	}
}
#include "Circle.h"

void Draw8Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;
	// Points 1
	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	// Points 2
	new_x = xc - x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	// Points 3
	new_x = xc + x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	// Points 4
	new_x = xc - x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	// Points 5
	new_x = xc + y;
	new_y = yc + x;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	// Points 6
	new_x = xc - y;
	new_y = yc + x;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	// Points 7
	new_x = xc + y;
	new_y = yc - x;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	// Points 8
	new_x = xc - y;
	new_y = yc - x;
	SDL_RenderDrawPoint(ren, new_x, new_y);
}

void BresenhamDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{
	int x = 0; int y = R;
	int p = 3 - 2 * R;
	while (x <= y)
	{
		Draw8Points(xc, yc, x, y, ren);
		if (p <= 0) 
		{
			p +=  4 * x + 6;
		}
		else
		{
			p += 4 * (x - y) + 10;
			y = y - 1;
		}
		x = x + 1;
	}
}

void MidpointDrawCircle(int xc, int yc, int R, SDL_Renderer *ren)
{

	int x = 0; int y = R;
	int p = 1 - R;
	while (x <= y)
	{
		Draw8Points(xc, yc, x, y, ren);
		if (p <= 0)
		{
			p += 2 * x + 3;
		}
		else
		{
			p += 2 * (x - y) + 5;
			y = y - 1;
		}
		x = x + 1;
		
	}
}

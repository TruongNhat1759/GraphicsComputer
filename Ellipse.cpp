#include "Ellipse.h"
#include <cmath>

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	int new_x;
	int new_y;
	// Points 1
	new_x = xc + x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	// Points 2
	new_x = xc + x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	// Points 3
	new_x = xc - x;
	new_y = yc + y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
	// Points 4
	new_x = xc - x;
	new_y = yc - y;
	SDL_RenderDrawPoint(ren, new_x, new_y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int x, y;
	float p;
	float z = (float)(pow(a, 4) / (pow(a, 2) + pow(b, 2)));
	// Area 1
	p = -2 * a*a*b + a*a + 2 * b*b;
	x = 0; 
	y = b;
	while (x <= sqrt(z))
	{
		Draw4Points(xc, yc, x, y, ren);
		if (p <= 0)
		{
			p += 2 * b*b*(2*x + 3);
		}
		else 
		{
			p += 2 * b*b*(2*x + 3) + 4 * a*a - 4 * a*a*y;
			y = y - 1;
		}
		x = x + 1;
	}
	// Area 2
	p = -2 * a*b*b + 2*a*a + b*b;
	x = a;
	y = 0;	
	while (x > sqrt(z))
	{
		Draw4Points(xc, yc, x, y, ren);
		if (p <= 0)
		{
			p += 2 * a*a*(2*y + 3);
		}
		else
		{
			p +=  4 * a*a*y - 4 * b*b*x;
			x = x - 1;
		}
		y = y + 1;
	}

}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	int x, y;
	float p;
	float z = (float)(pow(a, 4) / (pow(a, 2) + pow(b, 2)));
	// Area 1
	p = b*b - a*a*b + a*a/4.0;
	x = 0;
	y = b;
	while (x <= sqrt(z))
	{
		Draw4Points(xc, yc, x, y, ren);
		if (p <= 0)
		{
			p += b*b*(2 * x + 3);
		}
		else
		{
			p += b*b*(2 * x + 3) + 2*a*a*(-y + 1);
			y = y - 1;
		}
		x = x + 1;
	}
	// Area 2
	p = a*a - b*b*a + b*b / 4.0;
	x = a;
	y = 0;
	while (x > sqrt(z))
	{
		Draw4Points(xc, yc, x, y, ren);
		if (p <= 0)
		{
			p += a*a*(2 * y + 3);
		}
		else
		{
			p += a*a*(2 * y + 3) + 2 * b*b*(-x + 1);
			x = x - 1;
		}
		y = y + 1;
	}
}
